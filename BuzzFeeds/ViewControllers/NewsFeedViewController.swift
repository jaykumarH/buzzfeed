//
//  ViewController.swift
//  BuzzFeeds
//
//  Created by Jay on 2018-02-16.
//  Copyright © 2018 Jay. All rights reserved.
//

import UIKit

class NewsFeedViewController: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var newsFeedTableView: UITableView!
    @IBOutlet weak var noResultLabel: UILabel!
    private var selectedIndex: IndexPath?
    private let refreshControl = UIRefreshControl()

    //MARK: Constant
    private struct Constants {
        static let cellIdentifier = "NewsFeedTableViewCell"
        static let nibIdentifier = "NewsFeedTableViewCell"
        static let estimatedRowHeight:CGFloat = 210.0
        static let viewTitle = "Buzz Feeds"
        static let detailViewControllerID = "FeedDetailViewController"
        static let refreshTitle = "Refreshing Feed..."
    }

    //MARK: Properties
    private var presenter = NewsFeedViewControllerPresenter()

    //MARK: Default
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.newsFeedView = self
        presenter.viewReady()
    }
}

extension NewsFeedViewController {

    //MARK: Custom Methods
    private func setupTable(isHidden: Bool, errorMessage: String?) {
        DispatchQueue.main.async {
            self.noResultLabel.text = errorMessage
            if isHidden {
                self.newsFeedTableView.isHidden = isHidden
                self.noResultLabel.isHidden = !isHidden
                self.newsFeedTableView.delegate = nil
                self.newsFeedTableView.dataSource = nil
            } else {
                self.newsFeedTableView.isHidden = isHidden
                self.noResultLabel.isHidden = !isHidden
                self.newsFeedTableView.refreshControl = self.refreshControl
                self.newsFeedTableView.delegate = self
                self.newsFeedTableView.dataSource = self
                self.newsFeedTableView.estimatedRowHeight = UITableViewAutomaticDimension
                self.newsFeedTableView.rowHeight = Constants.estimatedRowHeight
                self.newsFeedTableView.tableFooterView = UIView()
                self.setupRefreshControl()
                self.refreshControl.endRefreshing()
            }
        }
    }

    private func setupRefreshControl() {
        let attributes = [NSAttributedStringKey.font: UIFont(name: "HelveticaNeue-Bold", size: 15)!,
                          NSAttributedStringKey.foregroundColor: UIColor.white]
        self.refreshControl.tintColor = .white
        self.refreshControl.attributedTitle = NSAttributedString(string: Constants.refreshTitle, attributes: attributes)
        self.refreshControl.addTarget(self, action: #selector(self.refreshTable), for: .valueChanged)

    }

    private func updateTable(feedItem: FeedItemModel, indexPath: IndexPath) {
        presenter.rssFeeds.feedItems[indexPath.row] = feedItem
        newsFeedTableView.reloadRows(at: [indexPath], with: .automatic)
    }

    @objc private func refreshTable() {
        presenter.refreshView()
    }
}

extension NewsFeedViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellIdentifier) as? NewsFeedTableViewCell
            else {
                return UITableViewCell()
        }
        //FIXME: Remove force unwrapping
        return cell.configureCellWith(dataSource: presenter.rssFeeds!.feedItems, indexPath: indexPath)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let feedItem = presenter.rssFeeds.feedItems[indexPath.row]
        selectedIndex = indexPath
        let detailViewController = storyboard?.instantiateViewController(withIdentifier: Constants.detailViewControllerID) as? FeedDetailViewController
        detailViewController?.feedItem = feedItem
        detailViewController?.indexPath = indexPath
        detailViewController?.delegate = self
        navigationController?.pushViewController(detailViewController!, animated: true)
    }
}

extension NewsFeedViewController: NewsFeedViewProtocol {
    //MARK: NewsFeedViewProtocol Methods
    func setupView(isTableHidden: Bool, errorMessage: String?) {
        title = Constants.viewTitle
        newsFeedTableView.backgroundColor = .appThemeColor
        let nib = UINib(nibName: Constants.nibIdentifier, bundle: nil)
        newsFeedTableView.register(nib, forCellReuseIdentifier: Constants.cellIdentifier)
        setupTable(isHidden: isTableHidden, errorMessage: errorMessage)
    }
}


extension NewsFeedViewController: FeedDetailViewControllerProtocol {
    //MARK: FeedDetailViewControllerProtocol Method
    func selectedFeed(feedItem: FeedItemModel, indexPath: IndexPath) {
        updateTable(feedItem: feedItem, indexPath: indexPath)
    }
}

