//
//  FeedDetailViewController.swift
//  BuzzFeeds
//
//  Created by Jay on 2018-02-18.
//  Copyright © 2018 Jay. All rights reserved.
//

import UIKit
protocol FeedDetailViewControllerProtocol: class {
    func selectedFeed(feedItem: FeedItemModel, indexPath: IndexPath)
}

class FeedDetailViewController: UIViewController {

    //MARK: Constants
    struct Constants {
        static let alertTitle = "Errr!!"
        static let alertMessage = "Web page could not be loaded!"
    }

    //MARK: Outlets and properties
    @IBOutlet weak var feedWebView: UIWebView!
    var feedItem: FeedItemModel!
    var indexPath: IndexPath!

    weak var delegate: FeedDetailViewControllerProtocol?

    //MARK: Default
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK: Custom Methods
    private func setupView() {
        guard let urlString = feedItem?.link,
            let validURL = URL(string: urlString) else {
                return
        }
        feedWebView.delegate = self
        let urlRequest = URLRequest(url: validURL)
        CommonSpinner.shared.showAnimating()
        feedWebView.loadRequest(urlRequest)
        title = feedItem?.title
    }
}

extension FeedDetailViewController: UIWebViewDelegate {
    //MARK: WebView Delegate
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        CommonSpinner.shared.hideAnimating()
        if !webView.isLoading {
            feedItem?.isRead = true
            delegate?.selectedFeed(feedItem: feedItem, indexPath: indexPath)
        }
    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        CommonSpinner.shared.hideAnimating()
        let code = (error as NSError).code
        if code != -999 {
            presentAlert(title: Constants.alertTitle, message: Constants.alertMessage)
            feedItem?.isRead = false
            delegate?.selectedFeed(feedItem: feedItem, indexPath: indexPath)
        }
    }
}
