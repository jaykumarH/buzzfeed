//
//  NetworkAdapter.swift
//  BuzzFeeds
//
//  Created by Jay on 2018-02-17.
//  Copyright © 2018 Jay. All rights reserved.
//

import Alamofire
import AlamofireImage
import SWXMLHash

typealias FeedsHandler = (_ rssFeeds: RSSFeedModel) -> Void
typealias ErrorHandler = (_ error: NSError) -> Void
typealias ImageHandler = (_ image: Image) -> Void

enum ErrorCode: Int {
    case parsingFail = 101 , responseFail, networkUnavailable, imageFetchFail
}
class NetworkAdapter {

    struct Constants {
        static let feedUrl = "https://www.ctvnews.ca/rss/ctvnews-ca-top-stories-public-rss-1.822009"
        static let rssKey = "rss"
        static let channelKey = "channel"
        static let responseError = "Respnse Fail"
        static let internetNotAvailable = "Network not available. Please check your internet connection"
        static let parsingFail = "Parsing Fail"
        static let imageFetchFail = "Image Fetch Fail"
    }

    class func getRSSFeeds(success: @escaping FeedsHandler, failure: @escaping ErrorHandler) {
        CommonSpinner.shared.showAnimating()
        if isConnectedToInternet() {
            Alamofire.request(Constants.feedUrl).responseString { response in
                CommonSpinner.shared.hideAnimating()
                switch response.result {
                case .success:
                    guard let value = response.result.value else {
                        return
                    }
                    let xml = SWXMLHash.parse(value)
                    guard let rssFeeds: RSSFeedModel = try? xml[Constants.rssKey][Constants.channelKey].value() else {
                        failure(NSError(domain: Constants.parsingFail, code: ErrorCode.parsingFail.rawValue, userInfo: nil))
                        return
                    }
                    success(rssFeeds)
                    break
                case .failure:
                    failure(NSError(domain: Constants.responseError, code: ErrorCode.responseFail.rawValue, userInfo: nil))
                    break
                }
            }
        } else {
            CommonSpinner.shared.hideAnimating()
            failure(NSError(domain: Constants.internetNotAvailable, code: ErrorCode.networkUnavailable.rawValue, userInfo: nil))
        }
    }

    class func fetchImageWith(url: String, success: @escaping ImageHandler, failure: @escaping ErrorHandler) {
        if isConnectedToInternet() {
            Alamofire.request(url).responseImage { response in
                if let image = response.result.value {
                    success(image)
                } else {
                    failure(NSError(domain: Constants.imageFetchFail, code: ErrorCode.imageFetchFail.rawValue, userInfo: nil))
                }
            }
        } else {
            CommonSpinner.shared.hideAnimating()
            failure(NSError(domain: Constants.internetNotAvailable, code: ErrorCode.networkUnavailable.rawValue, userInfo: nil))
        }
    }

    class func isConnectedToInternet() -> Bool {
        guard let manager = NetworkReachabilityManager() else {
            return false
        }
        return manager.isReachable
    }
}
