//
//  ViewController.swift
//  BuzzFeeds
//
//  Created by Jay on 2018-02-19.
//  Copyright © 2018 Jay. All rights reserved.
//

import UIKit

extension UIViewController {

    func presentAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
