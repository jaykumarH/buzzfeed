//
//  NewsFeedTableViewCell.swift
//  BuzzFeeds
//
//  Created by Jay on 2018-02-16.
//  Copyright © 2018 Jay. All rights reserved.
//

import UIKit

class NewsFeedTableViewCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var newsFeedImageView: UIImageView!
    @IBOutlet weak var readImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var publishDateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    struct Constants {
        static let placeHolderImage = "placeholder"
        static let viewedText = "Viewed"
        static let readImageName = "eye"
    }

    //MARK: Default
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .appThemeColor
        titleLabel.textColor = .white
        publishDateLabel.textColor = .white
        descriptionLabel.textColor = .cellTitleColor
    }

    func configureCellWith(dataSource: [FeedItemModel], indexPath: IndexPath) -> Self {
        let feedItem = dataSource[indexPath.row]
        titleLabel.text = feedItem.title
        publishDateLabel.text = feedItem.publishDate.getElapsedInterval()
        newsFeedImageView.image = UIImage(named: Constants.placeHolderImage)
        descriptionLabel.text = feedItem.description
        if feedItem.isRead {
            readImageView.image = UIImage(named:Constants.readImageName)?.maskWithColor(color: .white)
        } else {
            readImageView.image = nil
        }
        NetworkAdapter.fetchImageWith(url: feedItem.imageURL, success: { (image) in
            self.newsFeedImageView.image = image
        }) { (error) in
            self.newsFeedImageView.image = UIImage(named: Constants.placeHolderImage)
        }
        return self
    }    
}
