//
//  RSSFeedModel.swift
//  BuzzFeeds
//
//  Created by Jay on 2018-02-17.
//  Copyright © 2018 Jay. All rights reserved.

import SWXMLHash

struct Constants {
    static let itemKey = "item"
    static let lastBuildDateKey = "lastBuildDate"
    static let titleKey = "title"
    static let linkKey = "link"
    static let enclosureKey = "enclosure"
    static let urlKey = "url"
    static let descriptionKey = "description"
    static let creditLineKey = "creditLine"
    static let guidKey = "guid"
    static let pubDateKey = "pubDate"
}

struct RSSFeedModel: XMLIndexerDeserializable {
    let lastBuildDate: Date
    var feedItems: [FeedItemModel]

    static func deserialize(_ node: XMLIndexer) throws -> RSSFeedModel {
        return try RSSFeedModel (
            lastBuildDate: node[Constants.lastBuildDateKey].value(),
            feedItems: node[Constants.itemKey].value()
        )
    }
}

struct FeedItemModel: XMLIndexerDeserializable {
    let title: String
    let link: String
    let imageURL: String
    let description: String
    let creditLine: String
    let guID: String
    let publishDate: Date
    var isRead: Bool

    static func deserialize(_ node: XMLIndexer) throws -> FeedItemModel {
        return try FeedItemModel (
            title: node[Constants.titleKey].value(),
            link: node[Constants.linkKey].value(),
            imageURL: node[Constants.enclosureKey].value(ofAttribute: Constants.urlKey),
            description: node[Constants.descriptionKey].value(),
            creditLine: node[Constants.creditLineKey].value(),
            guID: node[Constants.guidKey].value(),
            publishDate: node[Constants.pubDateKey].value(),
            isRead: false
        )
    }
}
