import UIKit

@objc class CommonSpinner: NSObject {
    
    //MARK: Shared instance
    static let shared = CommonSpinner()
    var spinner: SpinnerView?
    
    //MARK: Custom Methods
    func showAnimating() {
        spinner = SpinnerView()
        spinner?.startAnimating()
    }
    
    func hideAnimating() {
        spinner?.stopAnimating()
        spinner = nil
    }
    
    func showAnimatingWith(style: SpinnerView.SpinnerStyle) {
        spinner = SpinnerView()
        spinner?.style = style
        spinner?.startAnimating()
    }
}

class SpinnerView: UIView {
    
    @objc enum SpinnerStyle : Int {
        case blue, gray
    }
    
    //MARK: Constants
    struct Constants {
        static let lineWidth: CGFloat = 5.0
        static let strokeEnd : CGFloat = 0.5
        static let animationDuration : CGFloat = 2.0
        static let width: CGFloat = 80.0
        static let height: CGFloat = 80.0
        static let animationKeypath = "transform.rotation.z"
        static let rotationAnimationKey = "rotateOuter"
    }
    
    //MARK: Properties
    var style : SpinnerStyle = .blue
    private var hidesWhenStopped = false
    private var fillColor = UIColor.clear
    private var strokeColor = UIColor.black
    private var circularRingView = UIView()
    
    //MARK: Init
    init() {
        let bounds = UIScreen.main.bounds
        let x = bounds.size.width / 2 - Constants.width / 2
        let y = bounds.size.height / 2 - Constants.height / 2
        let frame = CGRect(x: x, y: y, width: Constants.width, height: Constants.height)
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    
    override func draw(_ rect: CGRect) {
        
        switch style {
        case .blue:
            strokeColor = UIColor.appThemeColor
            break
        case .gray:
            strokeColor = UIColor.gray
            break
        }
        drawCircularRing(rect)
        self.startAnimating()
    }
}

extension SpinnerView {
    
    //MARK: Helper Methods
    private func commonInit() {
        self.backgroundColor = UIColor.clear
        UIApplication.shared.keyWindow?.addSubview(self)
    }
    
    private func animateCircularRing() {
        let rotationAnimation = CABasicAnimation(keyPath: Constants.animationKeypath)
        rotationAnimation.fromValue = 0 * CGFloat(Double.pi/180)
        rotationAnimation.toValue = 360 * CGFloat(Double.pi/180)
        rotationAnimation.duration = Double(Constants.animationDuration)
        rotationAnimation.repeatCount = HUGE
        circularRingView.layer.add(rotationAnimation, forKey: Constants.rotationAnimationKey)
    }
    
    private func drawCircularRing(_ rect: CGRect) {
        circularRingView.frame = CGRect(x: 0, y: 0, width: rect.size.width, height: rect.size.height)
        if let superview = self.superview {
            circularRingView.center = self.convert(self.center, from: superview)
        }
        let circularRingLayer = CAShapeLayer()
        circularRingLayer.path = UIBezierPath(ovalIn: circularRingView.bounds).cgPath
        circularRingLayer.lineWidth = Constants.lineWidth
        circularRingLayer.strokeStart = 0.0
        circularRingLayer.strokeEnd = Constants.strokeEnd
        circularRingLayer.lineCap = kCALineCapRound
        circularRingLayer.fillColor = fillColor.cgColor
        circularRingLayer.strokeColor = strokeColor.cgColor
        circularRingView.layer.addSublayer(circularRingLayer)
        self.addSubview(circularRingView)
    }
    
    fileprivate func startAnimating() {
        self.isHidden = false
        DispatchQueue.main.async {
            //UIApplication.shared.beginIgnoringInteractionEvents()
            self.animateCircularRing()
        }
    }
    
    fileprivate func stopAnimating() {
        if hidesWhenStopped {
            self.isHidden = true
        }
        
        DispatchQueue.main.async {
           //UIApplication.shared.endIgnoringInteractionEvents()
            self.circularRingView.layer.removeAllAnimations()
            self.removeFromSuperview()
        }
    }
}
