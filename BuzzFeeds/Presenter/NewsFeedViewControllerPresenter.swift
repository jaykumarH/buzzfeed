//
//  NewsFeedViewControllerPresenter.swift
//  BuzzFeeds
//
//  Created by Jay on 2018-02-17.
//  Copyright © 2018 Jay. All rights reserved.
//

import Foundation
import Alamofire
protocol NewsFeedViewProtocol: class {
    func setupView(isTableHidden: Bool, errorMessage: String?)
}

class NewsFeedViewControllerPresenter {
    
    //MARK: Properties
    weak var newsFeedView: NewsFeedViewProtocol?
    var rssFeeds: RSSFeedModel!
    private var lastBuildDate: Date!
    
    //MARK: Constant
    private struct Constants {
        static let errorMessage = "Oops..Something went wrong!!"
    }

    //MARK: Custom Methods
    func viewReady() {
        NetworkAdapter.getRSSFeeds(success: { (rssFeeds) in
            let feeds = rssFeeds.feedItems.sorted{ $0.publishDate > $1.publishDate }
            var rssFeeds = rssFeeds
            rssFeeds.feedItems = feeds
            self.rssFeeds = rssFeeds
            self.lastBuildDate = rssFeeds.lastBuildDate
            self.rssFeeds.feedItems.count > 0 ? self.newsFeedView?.setupView(isTableHidden: false,errorMessage: nil) : self.newsFeedView?.setupView(isTableHidden: true,errorMessage: Constants.errorMessage)
        }) { (error) in
            self.handleError(error: error)
        }
    }

    func numberOfRows() -> Int {
        guard let rssFeeds = rssFeeds else {
            return 0
        }
        return rssFeeds.feedItems.count
    }
    
    func refreshView() {
        NetworkAdapter.getRSSFeeds(success: { (rssFeeds) in
            if let diffInMinutes = Calendar.current.dateComponents([.day], from: rssFeeds.lastBuildDate, to: self.lastBuildDate).minute, diffInMinutes > 0  {
                self.rssFeeds = rssFeeds
            }
            self.rssFeeds.feedItems.count > 0 ? self.newsFeedView?.setupView(isTableHidden: false,errorMessage: nil) : self.newsFeedView?.setupView(isTableHidden: true,errorMessage: Constants.errorMessage)
        }) { (error) in
            self.handleError(error: error)
        }
    }

    private func handleError(error: NSError) {
        let nsError = (error as NSError)
        let code = nsError.code
        if code == ErrorCode.networkUnavailable.rawValue {
            self.newsFeedView?.setupView(isTableHidden: true,errorMessage: nsError.domain)
        } else {
            self.newsFeedView?.setupView(isTableHidden: true,errorMessage: Constants.errorMessage)
        }
    }
}
